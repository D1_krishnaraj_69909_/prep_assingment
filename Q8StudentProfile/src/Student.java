public class Student {
    String StudentName;
    String RollNo;
    int Marks;

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public String getRollNo() {
        return RollNo;
    }

    public void setRollNo(String rollNo) {
        RollNo = rollNo;
    }

    public int getMarks() {
        return Marks;
    }

    public void setMarks(int marks) {
        Marks = marks;
    }

    public void Display ( )
    {
        System.out.println("Name of Student = "+getStudentName());
        System.out.println("Roll No = "+getRollNo());
        System.out.println("Marks Obtained = "+getMarks());
    }

}
