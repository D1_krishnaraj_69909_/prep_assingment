import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int decimal;
        System.out.println("Enter the Decimal No : ");
        decimal = scanner.nextInt();

        String binary,octal,hexadecimal;

        binary= Integer.toBinaryString(decimal);
        octal = Integer.toOctalString(decimal);
        hexadecimal= Integer.toHexString(decimal);

        System.out.println("Binary No. Equivalent to decimal no : "+decimal + " is = "+binary);
        System.out.println("Octal No. Equivalent to decimal no : "+decimal + " is = "+octal);
        System.out.println("Hexadecimal No. Equivalent to decimal no : "+decimal + " is = "+hexadecimal);

    }
}