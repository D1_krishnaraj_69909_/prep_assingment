//import java.util.Scanner;Q3. Write a program to calculate Fibonacci Series up to n numbers
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        int n = scanner.nextInt();
        int a=0,b=1,c;

        for (int i = 1; i<=n; i++)
        {

            System.out.println(a +" ");
            c = a + b;
            a=b;
            b=c;

        }
    }
}