//Q12. Write a Program to reverse the letters present in the given String.

public class Main {
    public static void main(String[] args)
    {
        String name = "Krishnaraj";
        int length= name.length();
        String rev= "";

        for (int i = length-1; i>=0;i--)
        {
            rev= rev + name.charAt(i);
        }
        System.out.println("reversed string of name "+ name + " is = "+rev);
    }
}