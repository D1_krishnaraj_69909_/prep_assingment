import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        char ch;
        System.out.println("Enter the Key From Keyboard");
        Scanner r = new Scanner (System.in);
        ch = r.next().charAt(0);
    if (ch >= 65 && ch <= 90) {
        System.out.println("UpperCase");
    } else if (ch >= 97 && ch <= 122) {
        System.out.println("LowerCase");
    } else if (ch >= 48 && ch <= 57) {
        System.out.println("Numeric Key");
    } else
    {
        System.out.println("Special Characters");
    }
}


}