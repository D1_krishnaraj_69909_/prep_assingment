//Q14 Write a program to check if give string is palindrome.

public class Main {
    public static void main(String[] args) {
        String name = "Krish";
        String rev = "";
        int length = name.length();
        for (int i = length-1; i>=0; i--)
        {
            rev= rev + name.charAt(i);
        }

        if (name.equals(rev))
        {
            System.out.println("String is Palindrome");
        }
        else
        {
            System.out.println("String is Not Palindrome");
        }
    }
}