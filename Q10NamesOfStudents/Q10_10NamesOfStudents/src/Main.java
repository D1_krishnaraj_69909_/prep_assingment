//Q10. Read at most 10 names of students and store them into an array of
//String nameOfStudents[10]. Sort the array and display them back. Hint: Use
//Arrays.sort() method.

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String []nameOfStudents = new String[10];
        System.out.println("Enter 10 names");
        Scanner scanner = new Scanner(System.in);
        for(int i = 0; i< nameOfStudents.length ; i++) {
            nameOfStudents[i] = scanner.next();
        }
        Arrays.sort(nameOfStudents);

        for(int j = 0; j< nameOfStudents.length; j++){
            System.out.println(nameOfStudents[j]);
        }
    }
}